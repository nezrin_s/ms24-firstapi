package org.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/msclear")
public class FirstApi {

    @GetMapping("/hello")
    public String first(){
        return "Hello World";
    }
}
